<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab Menu*/
$lang['tab_news'] = 'Noticias';
$lang['tab_forum'] = 'Forum';
$lang['tab_store'] = 'Loja';
$lang['tab_bugtracker'] = 'Bugtracker';
$lang['tab_changelogs'] = 'Changelogs';
$lang['tab_pvp_statistics'] = 'Estatísticas PvP';
$lang['tab_login'] = 'Entrar';
$lang['tab_register'] = 'Registrar';
$lang['tab_home'] = 'Home';
$lang['tab_donate'] = 'Donate';
$lang['tab_vote'] = 'Votar';
$lang['tab_cart'] = 'Carrinho';
$lang['tab_account'] = 'Minha Conta';
$lang['tab_reset'] = 'Recuperação de senha';
$lang['tab_error'] = 'Error 404';
$lang['tab_maintenance'] = 'Manutenção';
$lang['tab_online'] = 'Jogadores Online';

/*Panel Navbar*/
$lang['navbar_vote_panel'] = 'Painel de Votação';
$lang['navbar_donate_panel'] = 'Painel de Doações';

/*Button Lang*/
$lang['button_register'] = 'Registrar';
$lang['button_login'] = 'Entrar';
$lang['button_logout'] = 'Sair';
$lang['button_forgot_password'] = 'Esqueceu sua senha?';
$lang['button_user_panel'] = 'Painel do Usuário';
$lang['button_admin_panel'] = 'Painel de administração';
$lang['button_mod_panel'] = 'Mod Panel';
$lang['button_change_avatar'] = 'Alterar Avatar';
$lang['button_add_personal_info'] = 'Adicionar informações pessoais';
$lang['button_create_report'] = 'Criar relatório';
$lang['button_new_topic'] = 'Novo Tópico';
$lang['button_edit_topic'] = 'Editar Tópico';
$lang['button_save_changes'] = 'Salvar alterações';
$lang['button_cancel'] = 'Cancelar';
$lang['button_send'] = 'Enviar';
$lang['button_read_more'] = 'Ler mais';
$lang['button_add_reply'] = 'Adicionar resposta';
$lang['button_remove'] = 'Remover';
$lang['button_create'] = 'Criar';
$lang['button_save'] = 'Salvar';
$lang['button_close'] = 'Fechar';
$lang['button_reply'] = 'Responder';
$lang['button_donate'] = 'Doar';
$lang['button_account_settings'] = 'Configurações da conta';
$lang['button_cart'] = 'Adicionar ao carrinho';
$lang['button_view_cart'] = 'Ver Carrinho';
$lang['button_checkout'] = 'Finalizar compra';
$lang['button_buying'] = 'Continuar comprando';

/*Alert Lang*/
$lang['alert_successful_purchase'] = 'Item comprado com sucesso.';
$lang['alert_upload_error'] = 'Sua imagem deve estar no formato jpg ou png';
$lang['alert_changelog_not_found'] = 'O servidor não tem changelogs para informar neste momento';
$lang['alert_points_insufficient'] = 'Pontos insuficientes';

/*Status Lang*/
$lang['offline'] = 'Offline';
$lang['online'] = 'Online';

/*Label Lang*/
$lang['label_open'] = 'Aberto';
$lang['label_closed'] = 'Fechado';

/*Form Label Lang*/
$lang['label_login_info'] = 'Informação de Login';

/*Input Placeholder Lang*/
$lang['placeholder_username'] = 'Nome de usuário';
$lang['placeholder_email'] = 'Endereço de e-mail';
$lang['placeholder_password'] = 'Senha';
$lang['placeholder_re_password'] = 'Repetir Senha';
$lang['placeholder_current_password'] = 'Senha atual';
$lang['placeholder_new_password'] = 'Nova Senha';
$lang['placeholder_new_email'] = 'Novo e-mail';
$lang['placeholder_confirm_email'] = 'Confirmar Novo Email';
$lang['placeholder_create_bug_report'] = 'Criar Relatório de Bug';
$lang['placeholder_title'] = 'Título';
$lang['placeholder_type'] = 'Tipo';
$lang['placeholder_description'] = 'Descrição';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_uri'] = 'URL amigável (Example: tos)';
$lang['placeholder_highl'] = 'Destaque';
$lang['placeholder_lock'] = 'Bloquear';
$lang['placeholder_subject'] = 'Assunto';

/*Table header Lang*/
$lang['table_header_name'] = 'Nome';
$lang['table_header_faction'] = 'Facção';
$lang['table_header_total_kills'] = 'Total de Mortes';
$lang['table_header_today_kills'] = 'Mortes de Hoje';
$lang['table_header_yersterday_kills'] = 'Mortes de Ontem';
$lang['table_header_team_name'] = 'Nome da equipe';
$lang['table_header_members'] = 'Membros';
$lang['table_header_rating'] = 'Classificação';
$lang['table_header_games'] = 'Jogos';
$lang['table_header_id'] = 'ID';
$lang['table_header_status'] = 'Status';
$lang['table_header_priority'] = 'Prioridade';
$lang['table_header_date'] = 'Data';
$lang['table_header_author'] = 'Autor';
$lang['table_header_time'] = 'Tempo';
$lang['table_header_icon'] = 'Ícone';
$lang['table_header_realm'] = 'Reino';
$lang['table_header_zone'] = 'Zona';
$lang['table_header_character'] = 'Personagem';
$lang['table_header_price'] = 'Preço';
$lang['table_header_item_name'] = 'Nome do item';
$lang['table_header_items'] = 'Item(s)';
$lang['table_header_quantity'] = 'Quantidade';

/*Class Lang*/
$lang['class_warrior'] = 'Guerreiro';
$lang['class_paladin'] = 'Paladino';
$lang['class_hunter'] = 'Caçador';
$lang['class_rogue'] = 'Ladino';
$lang['class_priest'] = 'Sacerdote';
$lang['class_dk'] = 'Cavaleiro da Morte';
$lang['class_shamman'] = 'Xamã';
$lang['class_mage'] = 'Mago';
$lang['class_warlock'] = 'Bruxo';
$lang['class_monk'] = 'Monge';
$lang['class_druid'] = 'Druida';
$lang['class_demonhunter'] = 'Caçador de Demónios';

/*Faction Lang*/
$lang['faction_alliance'] = 'Aliança';
$lang['faction_horde'] = 'Horda';

/*Gender Lang*/
$lang['gender_male'] = 'Masculino';
$lang['gender_female'] = 'Feminino';

/*Race Lang*/
$lang['race_human'] = 'Humano';
$lang['race_orc'] = 'Orc';
$lang['race_dwarf'] = 'Anão';
$lang['race_night_elf'] = 'Elfo Noturno';
$lang['race_undead'] = 'Morto-Vivo';
$lang['race_tauren'] = 'Tauren';
$lang['race_gnome'] = 'Gnomo';
$lang['race_troll'] = 'Troll';
$lang['race_goblin'] = 'Goblin';
$lang['race_blood_elf'] = 'Elfo Sangrento';
$lang['race_draenei'] = 'Draenei';
$lang['race_worgen'] = 'Worgen';
$lang['race_panda_neutral'] = 'Pandaren Neutro';
$lang['race_panda_alli'] = 'Pandaren Aliança';
$lang['race_panda_horde'] = 'Pandaren Horda';
$lang['race_nightborde'] = 'Filhos da Noite';
$lang['race_void_elf'] = 'Elfo Caótico';
$lang['race_lightforged_draenei'] = 'Draenei forjado a luz';
$lang['race_highmountain_tauren'] = 'Tauren Altamontês';
$lang['race_dark_iron_dwarf'] = 'Anões Ferro Negro';
$lang['race_maghar_orc'] = 'Maghar Orc';

/*Header Lang*/
$lang['header_cookie_message'] = 'Este site usa cookies para garantir que você obtenha a melhor experiência em nosso site. ';
$lang['header_cookie_button'] = 'Entendi!';

/*Footer Lang*/
$lang['footer_rights'] = 'Todos os direitos reservados.';

/*Page 404 Lang*/
$lang['page_404_title'] = '404 Page not found';
$lang['page_404_description'] = 'Parece que a página que você está procurando não pôde ser encontrada';

/*Panel Lang*/
$lang['panel_acc_rank'] = 'Rank da conta';
$lang['panel_dp'] = 'Pontos de Doação';
$lang['panel_vp'] = 'Pontos de Voto';
$lang['panel_expansion'] = 'Expansão';
$lang['panel_member'] = 'Membro desde';
$lang['panel_chars_list'] = 'Lista de Personagens';
$lang['panel_account_details'] = 'Detalhes da conta';
$lang['panel_last_ip'] = 'Último IP';
$lang['panel_change_email'] = 'Alterar endereço de e-mail';
$lang['panel_change_password'] = 'Alterar Senha';
$lang['panel_replace_pass_by'] = 'Substitua a senha por';
$lang['panel_current_email'] = 'Endereço de e-mail atual';
$lang['panel_replace_email_by'] = 'Substituir E-mail por';

/*Home Lang*/
$lang['home_latest_news'] = 'Últimas Notícias';
$lang['home_discord'] = 'Discord';
$lang['home_server_status'] = 'Status do Servidor';
$lang['home_realm_info'] = 'Atualmente o reino é';

/*PvP Statistics Lang*/
$lang['statistics_top_20'] = 'TOP 20';
$lang['statistics_top_2v2'] = 'TOP 2V2';
$lang['statistics_top_3v3'] = 'TOP 3V3';
$lang['statistics_top_5v5'] = 'TOP 5V5';

/*News Lang*/
$lang['news_recent_list'] = 'Lista de notícias recentes';
$lang['news_comments'] = 'Comentários';

/*Bugtracker Lang*/
$lang['bugtracker_report_notfound'] = 'Relatórios não encontrados';

/*Donate Lang*/
$lang['donate_get'] = 'Adquirir';

/*Vote Lang*/
$lang['vote_next_time'] = 'Próximo voto:';

/*Forum Lang*/
$lang['forum_posts_count'] = 'Posts';
$lang['forum_topic_locked'] = 'Este tópico está bloqueado.';
$lang['forum_comment_locked'] = 'Tens algo a dizer? Inicie sessão para se juntar à conversa.';
$lang['forum_comment_header'] = 'Participe da Conversa';
$lang['forum_not_authorized'] = 'Não autorizado';
$lang['forum_post_history'] = 'Ver Histórico de Mensagens';
$lang['forum_topic_list'] = 'Lista de Tópicos';
$lang['forum_last_activity'] = 'Última atividade';
$lang['forum_last_post_by'] = 'Último post por';
$lang['forum_whos_online'] = 'Quantos Online';
$lang['forum_replies_count'] = 'Respostas';
$lang['forum_topics_count'] = 'Tópicos';
$lang['forum_users_count'] = 'Usuários';

/*Store Lang*/
$lang['store_categories'] = 'Categorias da loja';
$lang['store_top_items'] = 'TOP Items';
$lang['store_cart_added'] = 'Você adicionou';
$lang['store_cart_in_your'] = 'no seu carrinho de compras';
$lang['store_cart_no_items'] = 'Você não tem itens em seu carrinho.';

/*Soap Lang*/
$lang['soap_send_subject'] = 'Compra de Loja';
$lang['soap_send_body'] = 'Obrigado por comprar na nossa loja!';

/*Email Lang*/
$lang['email_password_recovery'] = 'Recuperação de senha';
$lang['email_account_activation'] = 'Ativação de conta';
