<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Notification Title Lang*/
$lang['notification_title_success'] = 'Успешно';
$lang['notification_title_warning'] = 'Предупреждение';
$lang['notification_title_error'] = 'Ошибка';
$lang['notification_title_info'] = 'Информация';

/*Notification Message (Login/Register) Lang*/
$lang['notification_username_empty'] = 'Имя пользователя не указано';
$lang['notification_email_empty'] = 'Адрес почты не указан';
$lang['notification_password_empty'] = 'Пароль не указан';
$lang['notification_user_error'] = 'Неверное имя пользователя или пароль. Пожалуйста, попробуйте снова!';
$lang['notification_email_error'] = 'Неверный адрес почты или пароль. Пожалуйста, попробуйте снова!';
$lang['notification_check_email'] = 'Неверное имя пользователя или адрес почты. Пожалуйста, попробуйте снова!';
$lang['notification_checking'] = 'Проверка...';
$lang['notification_redirection'] = 'Подключение к вашей учётной записи...';
$lang['notification_new_account'] = 'Новая учётная запись создана. Перенаправление на авторизацию...';
$lang['notification_email_sent'] = 'Письмо отправлено. Пожалуйста, проверьте свою почту...';
$lang['notification_account_activation'] = 'Письмо отправлено. Пожалуйста, проверьте свою почту для активации учётной записи.';
$lang['notification_captcha_error'] = 'Пожалуйста, проверьте капчу';
$lang['notification_password_lenght_error'] = 'Неверная длина пароля. Пожалуйста, используйте пароль от 5 до 16 символов';
$lang['notification_account_already_exist'] = 'Эта учётная запись уже используется';
$lang['notification_password_not_match'] = 'Пароли не совпадают';
$lang['notification_same_password'] = 'Пароли совпадают.';
$lang['notification_currentpass_not_match'] = 'Старый пароль не совпадает';
$lang['notification_used_email'] = 'Электронная почта уже используется';
$lang['notification_email_not_match'] = 'Электронные почты не совпадают';
$lang['notification_expansion_not_found'] = 'Дополнение не найдено';
$lang['notification_valid_key'] = 'Учётная запись активирована';
$lang['notification_valid_key_desc'] = 'Теперь вы можете войти в свою учётную запись.';
$lang['notification_invalid_key'] = 'Указанный ключ активации недействителен.';

/*Notification Message (General) Lang*/
$lang['notification_email_changed'] = 'Email изменён.';
$lang['notification_password_changed'] = 'Пароль изменён.';
$lang['notification_avatar_changed'] = 'Аватар изменён.';
$lang['notification_wrong_values'] = 'Неверное значение';
$lang['notification_select_type'] = 'Выберете тип';
$lang['notification_select_priority'] = 'Выберете приоритет';
$lang['notification_select_category'] = 'Выберете категорию';
$lang['notification_select_realm'] = 'Выберете мир';
$lang['notification_select_character'] = 'Выберете персонажа';
$lang['notification_select_item'] = 'Выберете предмет';
$lang['notification_report_created'] = 'Отчет создан.';
$lang['notification_title_empty'] = 'Заголовок пуст';
$lang['notification_description_empty'] = 'Описание пусто';
$lang['notification_name_empty'] = 'Название пусто';
$lang['notification_id_empty'] = 'ID пуст';
$lang['notification_reply_empty'] = 'Ответ пуст';
$lang['notification_reply_created'] = 'Ответ отправлен.';
$lang['notification_reply_deleted'] = 'Ответ удалён.';
$lang['notification_topic_created'] = 'Тема создана.';
$lang['notification_donation_successful'] = 'Пожертвование было успешно завершено, проверьте свои очки пожертвования на своей учетной записи.';
$lang['notification_donation_canceled'] = 'Пожертвование было отменено.';
$lang['notification_donation_error'] = 'Информация, указанная в транзакции, не совпадает.';
$lang['notification_store_chars_error'] = 'Выберите своего персонажа в каждом предмете.';
$lang['notification_store_item_insufficient_points'] = 'У вас недостаточно очков, чтобы купить.';
$lang['notification_store_item_purchased'] = 'Предметы были куплены, пожалуйста, проверьте свою почту в игре.';
$lang['notification_store_item_added'] = 'Выбранный предмет был добавлен в вашу корзину.';
$lang['notification_store_item_removed'] = 'Выбранный предмет был удален из вашей корзины.';
$lang['notification_store_cart_error'] = 'Не удалось обновить корзину, попробуйте еще раз.';

/*Notification Message (Admin) Lang*/
$lang['notification_changelog_created'] = 'Список изменений создан.';
$lang['notification_changelog_edited'] = 'Список изменений отредактирован.';
$lang['notification_changelog_deleted'] = 'Список изменений удалён.';
$lang['notification_forum_created'] = 'Форум создан.';
$lang['notification_forum_edited'] = 'Форум отредактирован.';
$lang['notification_forum_deleted'] = 'Форум удалён.';
$lang['notification_category_created'] = 'Категория создана.';
$lang['notification_category_edited'] = 'Категория отредактирована.';
$lang['notification_category_deleted'] = 'Категория удалена.';
$lang['notification_menu_created'] = 'Меню создано.';
$lang['notification_menu_edited'] = 'Меню отредактировано.';
$lang['notification_menu_deleted'] = 'Меню удалено.';
$lang['notification_news_deleted'] = 'Новость удалена.';
$lang['notification_page_created'] = 'Страница создана.';
$lang['notification_page_edited'] = 'Страница отредактирована.';
$lang['notification_page_deleted'] = 'Страница удалена.';
$lang['notification_realm_created'] = 'Мир создан.';
$lang['notification_realm_edited'] = 'Мир изменён.';
$lang['notification_realm_deleted'] = 'Мир удалён.';
$lang['notification_slide_created'] = 'Слайд создан.';
$lang['notification_slide_edited'] = 'Слайд отредактирован.';
$lang['notification_slide_deleted'] = 'Слайд удалён.';
$lang['notification_item_created'] = 'Предмет создан.';
$lang['notification_item_edited'] = 'Предмет отредактирован.';
$lang['notification_item_deleted'] = 'Предмет удалён.';
$lang['notification_top_created'] = 'Топ предмет был создан.';
$lang['notification_top_edited'] = 'Топ предмет отредактирован.';
$lang['notification_top_deleted'] = 'Топ предмет удалён.';
$lang['notification_topsite_created'] = 'Топ создан.';
$lang['notification_topsite_edited'] = 'Топ отредактирован.';
$lang['notification_topsite_deleted'] = 'Топ удалён.';

$lang['notification_settings_updated'] = 'Настройки обновлены.';
$lang['notification_module_enabled'] = 'Модуль подключен.';
$lang['notification_module_disabled'] = 'Модуль отключен.';
$lang['notification_migration'] = 'Настройки установлены.';

$lang['notification_donation_added'] = 'Пожертвование добавлено';
$lang['notification_donation_deleted'] = 'Пожертвование удалено';
$lang['notification_donation_updated'] = 'Пожертвование обновлено';
$lang['notification_points_empty'] = 'Очки не заполнены';
$lang['notification_tax_empty'] = 'Пошлина не заполнена';
$lang['notification_price_empty'] = 'Цена не заполнена';
$lang['notification_incorrect_update'] = 'Неожиданное обновление';
$lang['notification_route_inuse'] = 'Маршрут уже используется, выберите другой.';

$lang['notification_account_updated'] = 'Учётная запись обновлена.';
$lang['notification_dp_vp_empty'] = 'DP/VP не заполнено';
$lang['notification_account_banned'] = 'Учётная запись заблокирована.';
$lang['notification_reason_empty'] = 'Причина не заполнена';
$lang['notification_account_ban_remove'] = 'Блокировка учётной записи снята.';
$lang['notification_rank_empty'] = 'Ранг не заполнен';
$lang['notification_rank_granted'] = 'Ранг предоставлен.';
$lang['notification_rank_removed'] = 'Ранг удалён.';

$lang['notification_cms_updated'] = 'CMS обновлена';
$lang['notification_cms_update_error'] = 'CMS не обновлена';
$lang['notification_cms_not_updated'] = 'Новая версия не найдена для обновления';
$lang['notification_select_category'] = 'Это не подкатегория';
